# Plan Date 22 June 2024 (デート)
============= RENCANA DAPAT BERUBAH SEWAKTU-WAKTU ============<br>

> Note: Harap jangan expect tinggi 😁, semoga semuanya lancar, gk trlalu cape kmunya dan terjangkau amin

> ⚠ Tempat makan (bates 30k 1 org + nasi, lebih murah lebih bagus, 50k msh ok trgantung tmpt, lebih dari itu skip)

> ⚠ Gak semua dikunjungin tapi kalo masih 1 lokasi, gass aj klo mau
 

| NO | Keterangan               | Tempat                           |
| -- | ------------------------ | -------------------------------- |
| 1  |  **Start** (Upcoming June 😁) | FX Sudirman CGV         |
| 2  | Jalan-jalan (kn sblh FX Sudirman GBK, jd jalan aj gk naik motor) | Hutan Kota GBK / [Fountain Area](https://www.instagram.com/reel/C74HUxHy7cE/?igsh=OHA0ZmpobzFhNDUy) / [Indonesia Open 2024 Istora Senayan GBK](https://bwfworldtour.bwfbadminton.com/tournament/4748/indonesia-open-2024/overview) |
| 3  | Liat event garffield + Hyundai Motors 4D Panel + Rooftop | SPARK |
| 4  | Sholat dzuhur & Makan | Masjid Jami Al-Bina GBK |
| 5  | Cabs naik motor ke Perpus | [Taman Ismail Marzuki](https://maps.app.goo.gl/nrgNUv4jc8wCGJdC7) / [The Foundation of Japan](https://maps.app.goo.gl/LKPVN4ZqeLA4j2KE6) / <ins> [Erasmus Huis](https://maps.app.goo.gl/rZQ4y6L5G666mVtK7) </ins> / [Gothe Institut Jakarta](https://maps.app.goo.gl/5s3TjUoHG7U77Qnn6) |
| 6  | Cabs naik motor ~~iseng aj liat layang², & event HUT Jakarta~~ / nyantai kulineran | ~~Ancol~~ / ~~[Taman Wisata Alam Mangrove](https://www.jakartamangrove.id/tiket.php)~~ / [Photobooth (FotoHokkie Blok M)](https://maps.app.goo.gl/LWxNZXNa5AqTsevV8) / [Korean Photobooth Plaza Blok M **(IG)**](https://www.instagram.com/p/C70c-oSpGYk/?igsh=cm94N2IwOThjYmt5) / [Jajanan Blok M **(IG)**](https://www.instagram.com/reel/C4PbzIUuTgl/?igsh=MTF4Ym02ZWdwemY3bg==) |
| 8  | Ashar & Makan   | [Masjid Nurul Iman Blok M](https://maps.app.goo.gl/9kBPW9CKKyWGiZf8A) |
| 7  | Nyari sewa sepeda keliling PIK (kalo ada makanan boleh beli) | [liat aloha / greenbelt / taman doa akita / land's ends / indonesia design district](https://www.instagram.com/p/C7f1zUhJrgo/?igsh=cXJzdmFkeHNkcndr) / [Wira Caffee PIK](https://maps.app.goo.gl/Rk7koJYCwxQY6oi28) |
| 9  | Cabs naik motor (Kobain Coffee sdikit mahal) | klo mau ke GI Grand Indonesia, spot foto di dpan HI (tpi mk tap in transjakarta) / nunggu magrib di [lapangan banteng (bnyk jajanan)](https://maps.app.goo.gl/UdVUHCbVrJhuyRis5) / ~~[Kobain Coffee Plaza Semanggi](https://maps.app.goo.gl/UcdtW8g3Z2U48uJLA)~~ ~~[[Harga & View KobainCoffee](https://www.instagram.com/reel/C63ZQ1IySwX/?igsh=MW54NGl6bHBxYzlncg==)]~~ / [Skydeck Sarinah](https://maps.app.goo.gl/2y6Y54afXceb27pn7) / ~~[Love on Top](https://maps.app.goo.gl/6bswCbFsyDxaM55AA)~~
| 10  | Sholat magrib | Istiqlal / Masjid terdekat |
| 11  | Beli bakpau & nikmatin HUT Jakarta 497 | Last Monas |
| 12  | **END** Anterin pulang | - |

**OPSIONAL** <br>
1. [Taman One Satrio Kuningan (kafe didlm taman)](https://maps.app.goo.gl/vrRygvhWz7jq4wi27) - [Review](https://www.instagram.com/reel/C6ZA7HUSY0y/?igsh=Z2cwbnc5dTRub2Q1)
2. [Taman Menteng (jajanan diblakang taman)](https://maps.app.goo.gl/okNch8RcuVKo2MWA6)
3. [Taman Suropati (jajanan disamping taman)](https://maps.app.goo.gl/MRCK76jYzLQ3Fwz7A)
4. [SPARK & Skydeck Sarinah & Rooftop Plaza Semanggi & Hutan Kota GBK & Rooftop PIM 3](https://www.instagram.com/p/C7oJ1HYO5VX/?igsh=dXB4Y2UxaW4zbWRq&img_index=5)
5. [klo mau nyoba speda PIK](https://maps.app.goo.gl/vcsv6mUGjvLcSW9G9) - ([Instagram](https://www.instagram.com/pikabike_/))
6. [Hanami & Hallyu Anime Fest Grand Indonesia](https://www.instagram.com/p/C73SkGISfkA/?igsh=em44NDN3a2tvcGlv)
7. [Konser Agustus - Deadline Tiket 12 Juni](https://www.instagram.com/p/C76jr0wyTta/?igsh=MXVvbms2OHFyc2Zzbg==)
8. [Sehari di PIK](https://www.instagram.com/reel/C5fT589yibO/?igsh=MWJpYmgwMWJzNGJhYg==)
9. [Ulang Tahun Jakarta](https://www.instagram.com/p/C7jUbHeRc2A/?igsh=bGUxdm1pdzlsdTNl)



## HUT Jakarta 497 tanggal 22 Juni
<img src="https://www.jakarta.go.id/storage/files/shares/Landing%20Page/HUTJakarta497/Section%204/horizontal3.png" data-canonical-src="https://www.jakarta.go.id" width="300" height="500" /> HUT Jakarta 497 22 Juni 2024
[https://www.jakarta.go.id/jakarta497#events](https://www.jakarta.go.id/jakarta497#events) <br>
Rangkaian acara HUT Jakarta 497 tanggal 22 Juni (kita ambil di monas nya aja)
- Jakarta Internasional Kite Festival **(Ancol)**
    - bentuk unik layang - layang, hingga lihat layang-layang yang super besar
- Gebyar Seni Budaya Betawi **(Perkampungan Budaya Betawi)**
- Jakarta Light Festival **(Kota Tua / Musim Fatahilah)**
- Malam Jaya Raya **(Monas)**
    - Akan ada panggung hiburan yang diisi oleh penyanyi kenamaan Indonesia, video mapping, drone show, kembang api, dan masih banyak lagi, yang hadir dalam satu momen kemeriahan hari ulang tahun kota Jakarta. Yang lebih menariknya lagi, hadir pula peragaan busana dari jajaran pemerintah provinsi kota Jakarta yang bisa disaksikan oleh semua yang hadir. 
- Wonders Of Jakarta (Konser Musik) **(Ancol)**
    - di acara Wonders of Jakarta di Ecopark Ancol
- Upacara Peringatan HUT Ke-497 Kota Jakarta **(Plaza Monas, 22 Juni 2024)**
- Pengumuman dan Pemberian Penghargaan Lomba Kelurahan Tingkat Provinsi DKI Jakarta **(Balaikota)**
- Malam Resepsi/Jamuan Dubes digabung dengan "Malam Jaya Raya" **(Monas)**

## Review View 
| Foto | Tempat |
| --------------------------- | ------------------ |
| ![SPARK Hyndai Motors](https://lh3.googleusercontent.com/p/AF1QipOXsuw80d096OD6qzstpsrPP7KOJ_k5ko80oBkU=s680-w680-h510) | [Spark Hyndai Motors](https://maps.app.goo.gl/1XYdmqL2N8fk9rM79)
| ![SPARK Rooftop](https://asset.kompas.com/crops/EItgUoKmnRoiDYsUUiwc-uTnSVg=/0x15:1600x1081/750x500/data/photo/2023/03/10/640acd7eeaf38.jpeg) | SPARK Rooftop (klo mau naik skywalk nya, (klo gkslh wajib beli sesuatu di SPARK)) |
| ![SPARK Event garffield](https://instagram.fcgk42-1.fna.fbcdn.net/v/t39.30808-6/445209339_18069316594518835_417597539809305451_n.jpg?stp=dst-jpg_e35_s1080x1080_sh0.08&_nc_ht=instagram.fcgk42-1.fna.fbcdn.net&_nc_cat=107&_nc_ohc=rHbNApkCHsgQ7kNvgFPyyJ9&edm=ANTKIIoAAAAA&ccb=7-5&oh=00_AYAAwYkUQVpI8FLavQ8i3JTsnTkfMWXKCJLVYWzyVJRGEw&oe=6660C11A&_nc_sid=cf751b) | [Garfield at SPARK](https://www.instagram.com/senayan.park/reel/C7wLYFiSJxW/) |
| ![FX Sudirman CGV](https://jadwalnonton.com/data/images/theaters/cgv-fx-sudirman_430x280.webp) ![Theater JKT48](https://static.promediateknologi.id/crop/0x0:0x0/750x500/webp/photo/p1/27/2024/05/23/intip-wajah-baru-theater-jkt48-di-jakarta-megah-banget-serasa-di-kerajaan25_700-3092611332.jpg) | [FX Sudirman CGV & Theater JKT48](https://maps.app.goo.gl/emHbmT8MQ7cfYC9i6) |
| ![Hutan Kota GBK](https://gallery.poskota.co.id/storage/Foto/HUTAAAANNANNANAN.JPG) | [Hutan Kota GBK (Gelora Bung Karno)](https://maps.app.goo.gl/pJyw79ntHUACZkNLA) |
| ![Masjid Jami Al-Bina](https://lh3.googleusercontent.com/p/AF1QipNff8AvT8fnQXRm_qsSJI0WTKxQ9UXLY49coi8f=s1360-w1360-h1020) | Masjid Jami Al-Bina GBK |
| ![Perpus Jakarta Taman Ismail Marzuki](https://akcdn.detik.net.id/visual/2022/07/18/wajah-baru-taman-ismail-marzuki-dengan-dibukanya-kembali-perpustakaan-jakartainstagramperpusjkt-5_169.jpeg?w=700&q=90) | [Perpus Jakarta Taman Ismail Marzuki](https://maps.app.goo.gl/nrgNUv4jc8wCGJdC7) |
| ![Perpus The Japan Fondation](https://www.jfkl.org.my/wp-content/uploads/2022/07/jlpt-worldwide.jpg) | [Perpus The Japan Foundation](https://maps.app.goo.gl/bcryZwT5ASGzcdkk9) |
| ![Erasmus Huis](https://jendeladuniaku2015.wordpress.com/wp-content/uploads/2022/03/rak-atas.jpeg) | [Erasmus Huis](https://maps.app.goo.gl/fRZEvBwYnQPacZY8A) |
| ![Masjid Nurul Iman Blok M](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvcjCZt5ogW-p5pZm6w978QmawlEcpkHBsrA&s) | [Masjid Nurul Iman Blok M](https://maps.app.goo.gl/1JrurWtkQK1CjNcs9) |
| ![Taman Wisata Mangrove](https://lh3.googleusercontent.com/p/AF1QipP2I_Cn4IrQBjfy9QpLS-pyU8bSIgzE0BgwEpEf=s1360-w1360-h1020) | [Taman Wisata Mangrove](https://maps.app.goo.gl/tL5psdo932RvwVDfA) |
| ![Pantjoran Chinatown PIK](https://akcdn.detik.net.id/community/media/visual/2022/01/27/pantjoran-pik-6_43.jpeg?w=700&q=90) | [Pantjoran Chinatown PIK](https://maps.app.goo.gl/6kUaeJg2NBnnL18M6) |
| ![Lapangan Banteng](https://asset.kompas.com/crops/VVS9ubSvknUDcKUYMvbzqkuLg3w=/0x52:1280x905/750x500/data/photo/2023/09/22/650db220ed18a.jpg) | [Lapangan Banteng Jakarta](https://maps.app.goo.gl/mfFyRajhESpPJ4LGA) |
| ![Dreamscape Lippo Mall Puri](https://cms.pasundanekspres.id/storage/uploads/conten/cqDdeK3mTNRagCtV.webp) | [Dreamscape Lt.1 Lippo Mall Puri](https://maps.app.goo.gl/m9ypKezFrD5J18m46) |
| ![La Riviera PIK 2](https://static.promediateknologi.id/crop/0x0:0x0/0x0/webp/photo/indizone/2023/02/04/XxsdqJQ/la-riviera-pik-2-menikmati-suasana-ala-di-kanal-belanda-enggak-jauh-dari-jakarta26.jpg) | [La Riviera PIK 2](https://maps.app.goo.gl/xx6ei8PAg6vGeFN27) |
| ![Monas HUT Jakarta](https://asset.kompas.com/crops/PhITM--ncHNsFxbN4fX4kRKV1bA=/0x0:4464x2976/750x500/data/photo/2023/08/16/64dcacdf08468.jpg) | Monas |

## Reference
|No | Tempat | Link |
| ---- | ------------- | ---------------------------------------------- |
| 1.   | Blok M        | [Coffee & Resto](https://www.instagram.com/reel/C6dj8nfvtw-/?igsh=eXNsMmRyaGNqb3pl) - [Coffee](https://www.instagram.com/reel/C55uGJBSR6m/?igsh=OW5veWpicXVoMGZx) - [Jus Murah](https://www.instagram.com/reel/C7s1s9aPriJ/?igsh=MWM2NnByaWRub25rNg==) - [Kuliner Makan](https://www.instagram.com/reel/C5FGyhHvbci/?igsh=MW52aWJ5MzM0ZGxyYQ==) - [Yoristudio Matalokal, Mblocspace 39k(start)](https://www.instagram.com/reel/C4SQHl4RDPp/?igsh=MWl5cW1nd2J4YjBvbA==) - [Kuliner Legend Blok M](https://www.instagram.com/reel/C6GWn91x7S5/?igsh=cGNlOWFtOGI3b2Z6) [Collection of Block M](https://www.instagram.com/p/C68dHxHJSYd/?igsh=MTExMXNrMGM5Njd0Mw==) - [Serba Serbi Block M](https://www.instagram.com/blokmsquare?igsh=MXI2bXBjc2twcjdzYQ==) - [Bakmie Makmur & Fumo Block M](https://www.instagram.com/reel/C6gZX-kPqXf/?igsh=ODk3NjdrdjJydzQw) |
| 2.   | PIK 2        | [Orange Grover PIK 2](https://www.instagram.com/reel/C6Vv3blylYz/?igsh=dGRmY3g2eHVtbzJw) - [Land's PIK - Festival layang-layang 30 Mei -2 juni](https://www.instagram.com/reel/C7siVaASUcM/?igsh=NzNicXNyMHdjNTls) - [PIK Coffee 2D](https://maps.app.goo.gl/T34ziLEqfhMqrXLK6) - [La Riviera](https://www.instagram.com/p/Ci3_BkwDrdh/?hl=en) |
| 3.   | ROH Project Jakpus        | [Pameran Seni Jakpus](https://www.instagram.com/reel/C4SsQZDyC_v/?igsh=aXIwaDIwazBhMGVl) |
| 4.   | AEON Mall        | [Gundala AEON Mall](https://www.instagram.com/reel/C4LCHyerFGH/?igsh=MXFwaG1weHRzaWhnag==) - [View](https://www.instagram.com/reel/C4-AGlaSapr/?igsh=YXlxMW9odXJ0Y2tz) |
| 5.   | Bundaran HI        | [Bundaran HI](https://www.instagram.com/reel/C554zoDxKFC/?igsh=OXo1eTVnYjFhZjh4) - [MRT Bundaran HI 1 - 30 Juni](https://www.instagram.com/reel/C7txyifSPkz/?igsh=MW5rdW5jZW5wOTk5ag==) |
| 6.   | Ashta District 8 SCBD        | [Spot Foto Background Gedung](https://www.instagram.com/reel/C5nNoCNSVCZ/?igsh=MTVrZTJxajZuZjdycA==)  |
| 6.   | Lotte Mall        | [Korean Style](https://maps.app.goo.gl/nE11Bp6M5KUBhTBW7)  |
| 7.   | Taman Ismail Marzuki        | [Perpustakaan](https://www.instagram.com/reel/C6D4FPsrDkl/?igsh=ODR2ZGkxazVxMjls) - [View Perpus](https://www.instagram.com/reel/C63JTx8xg2f/?igsh=MTY2aHd6NmJpbXB5Yg==)  |
| 8.   | Bank Indonesia        | [Perpustakaan](https://www.instagram.com/reel/C5A0N1dvhgW/?igsh=MTlsa2RsbzA3a285eg==)  |
| 9.   | Erasmus Hui        | [Perpustakaan](https://www.instagram.com/reel/C59zdomSxcV/?igsh=MW5zYXg4OXBtajUycQ==)  |
| 10.  | Grand Indonesia        | [Toko Buku](https://www.instagram.com/reel/C4xM9zUx99V/?igsh=eWcwaDA5aWg3bThw)  |
| 11.  | Mall Lippo Puri        | [Dreamscape di Lt1](https://www.instagram.com/reel/C5A9Tn0v87I/?igsh=b3AxcTV3cXBycmt0)  |
| 12.  | PIM        | [Fotohokkie](https://maps.app.goo.gl/MgwnUB1NbSbZ5nnJ6) - [Rooftop PIM 3](https://www.instagram.com/p/C7oJ1HYO5VX/?igsh=dXB4Y2UxaW4zbWRq&img_index=5)  |
| 13.  | Tutorial        | [Tutorial ke Jogja Keretaan 500k](https://www.instagram.com/reel/C7tYPqOvoNw/?igsh=MTQ1amllNnhycjRoMA==)  |
| 14.  | List Kopi Murah        | [Kopi Murah](https://www.instagram.com/p/C7Rh4n_S_gC/?igsh=YW1xdjRibW90a2Z2&img_index=6)  |